import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ReservationComponent } from './reservation/reservation.component';
import { GuestComponent } from './guest/guest.component';
import { RoomComponent } from './room/room.component';



const routes: Routes = [
  
  {path:'', component:LoginComponent},

  {path: 'reservation', component:ReservationComponent},

  {path: 'guest', component:GuestComponent},

  {path: 'room', component:RoomComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponent = [LoginComponent,ReservationComponent]
