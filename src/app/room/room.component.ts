import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Rooms } from '../Users';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss']
})
export class RoomComponent implements OnInit {

  roomList: Rooms[] = [];
  deleteID!: number;
  closeResult!: string;
  userForm!:FormGroup;
  pageLength!:any;
  page: number = 1;
  paginator: any;
  editForm!:FormGroup;
  editID!: number;


  constructor(private authService:AuthService, private modalService: NgbModal, private builder : FormBuilder,  private http: HttpClient) {
    this.userForm = this.builder.group({
      id:['',Validators.required],
      RoomNo:['',Validators.required],
      RoomTypeId:['',Validators.required],
      Price:['',Validators.required],
      RoomStatus:['',Validators.required],
      IsActive:['',Validators.required]
    })
    this.editForm = this.builder.group({
      id: [''],
      RoomNo: [''],
      RoomTypeId: [''],
      Price: [''],
      RoomStatus: [''],
      IsActive: [''],
    } );

   }

  ngOnInit(): void {
    this.getRooms("1");
  }

  getRooms(roomId:any){ 
    this.authService.roomList().subscribe((response) => {
          this.roomList = response;
          console.log(this.roomList[0].RoomTypeId == roomId)
          this.roomList = this.roomList.filter(item => item.RoomTypeId == roomId);
          this.pageLength = response.length;  

      },

      (error) => { console.log("error")}
    );
  }

  openDelete(targetModal: any, guestId: any) {
    this.deleteID = guestId;
    const modal = this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });
  }

  onDelete(deleteID: any) {
    console.log("id", this.deleteID)
    this.roomList.splice(this.deleteID - 1, 1)
    this.authService.delRoom(deleteID).subscribe((result)=>{
      console.log(result);
    
    })
    this.modalService.dismissAll();

  }

  open(content: any) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  addItems():void{
    this.authService.addNewRoom(this.userForm.value).subscribe(result => {
      console.log(result);
      alert(
        "data added"
      )
    })
    this.roomList.push(this.userForm.value);
    this.userForm.reset();
    this.modalService.dismissAll();
 }

 openStandard(){
    this.getRooms("1");
 }

 openPremium(){
  this.getRooms("2");
 }

 openDelux(){
  this.getRooms("3");
 }

 openExecutive(){
  this.getRooms("4");
 }

 openEdit(targetModal: any, room: Rooms ) {
    
  this.modalService.open(targetModal, {
    backdrop: 'static',
    size: 'lg'
  });

  this.editForm.patchValue( {
    id: room.id, 
    RoomNo: room.RoomNo,
    RoomTypeId: room.RoomTypeId,
    Price: room.Price,
    RoomStatus: room.RoomStatus,
    IsActive: room.IsActive
  });
}
saveEdit(roomId: any){
  const editURL = 'http://localhost:3000/rooms/'+ this.editForm.value.id ;
  console.log(this.editForm.value);
  this.http.put(editURL, this.editForm.value)
    .subscribe((results) => {
      this.ngOnInit();
      this.modalService.dismissAll();
    });
}

}

function data(data: any) {
  throw new Error('Function not implemented.');
}

