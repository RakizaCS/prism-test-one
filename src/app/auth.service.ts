import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Reservation } from './Users';
import { Rooms } from './Users';
import { Guest } from './Users';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  constructor(private http:HttpClient) { }

  // Data get from API to display in the tables

  login(data: any):Observable<any>{
    return this.http.get("http://localhost:3000/users", data);
  }

  reservationList(){
    return this.http.get<Reservation[]>("http://localhost:3000/orders");
  }

  roomList(){
    return this.http.get<Rooms[]>("http://localhost:3000/rooms");
  }

  guestList(){
    return this.http.get<Guest[]>("http://localhost:3000/contacts");
  }

  delGuest(deleteID: any){
    return this.http.delete("http://localhost:3000/contacts"+'/'+deleteID)
  }

  // Add new details to API

  addNew(data:any):Observable<any>{
    return this.http.post("http://localhost:3000/contacts" , data)
  }

  addNewReservation(data:any):Observable<any>{
    return this.http.post("http://localhost:3000/orders" , data)
  }

  addNewRoom(data:any):Observable<any>{
    return this.http.post("http://localhost:3000/rooms" , data)
  }

  // Delete Detail from API

  delReservation(deleteID: any){
    return this.http.delete("http://localhost:3000/orders"+'/'+deleteID)
  }

  delRoom(deleteID: any){
    return this.http.delete("http://localhost:3000/rooms"+'/'+deleteID)
  }
  
  // Calling the API to get relevant Room Type
  getStandard(){
    return this.http.get<Rooms[]>("http://localhost:3000/rooms");
  }
}

