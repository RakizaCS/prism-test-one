import { Component, OnInit} from '@angular/core';
import { AuthService } from '../auth.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { style } from '@angular/animations';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit{

  formGroup = new FormGroup({
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  })
  constructor(private authService:AuthService, private router: Router) { }

  ngOnInit() {}

  username1?:string;
  pwd1?:string;

  loginProcess(){

    if(this.formGroup.valid){
      this.authService.login(this.formGroup.value).subscribe(result => { 
       
      let isAuthValid:boolean = false;
      for(let i=0; i<2; i++){
        this.username1 = result[i].email;
        this.pwd1 = result[i].password;

        if(this.formGroup.value.email == this.username1 && this.formGroup.value.password == this.pwd1){
          isAuthValid = true;

          console.log("email and pwd is correct");
          this.router.navigate(['/reservation']);
          break;

        }
      }
      if(!isAuthValid){
        console.log("wrong")
        alert("wrong authenticatiom")
      }
      })
    }
  }


}

// function email(email: any) {
//   throw new Error('Function not implemented.');
// }

