import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Guest } from '../Users';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-guest',
  templateUrl: './guest.component.html',
  styleUrls: ['./guest.component.scss']
})

export class GuestComponent implements OnInit {

  userForm!:FormGroup
  editForm!:FormGroup
  closeResult!: string;
  guestList: Guest[] = [];
  deleteID!: number;
  editID!: number;
  resultGuest! : any;
  pageLength!:any;
  page: number = 1;
  paginator: any;

  constructor(private authService: AuthService, private modalService: NgbModal, private builder : FormBuilder, private http: HttpClient) {
    this.userForm = this.builder.group({
      GuestId:['',Validators.required],
      Name:['',Validators.required],
      PhoneNo:['',Validators.required],
      Email:['',Validators.required],
      IdNo:['',Validators.required],
      DateOfBirth:['',Validators.required],
      IsActive:['',Validators.required]
    })

    this.editForm = this.builder.group({
      GuestId: [''],
      Name: [''],
      PhoneNo: [''],
      Email: [''],
      IdNo: [''],
      DateOfBirth: [''],
      IsActive: ['']
    } );
   }

  //  Guest data edit 

  
   openEdit(targetModal: any, guest: Guest ) {
    
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });

    this.editForm.patchValue( {
      GuestId: guest.GuestId, 
      Name: guest.Name,
      PhoneNo: guest.PhoneNo,
      Email: guest.Email,
      IdNo: guest.IdNo,
      DateOfBirth: guest.DateOfBirth,
      IsActive: guest.IsActive
    });
  }

  saveEdit(guestId: any){
    const editURL = 'http://localhost:3000/contacts/'+ this.editForm.value.GuestId ;
    console.log(this.editForm.value);
    this.http.put(editURL, this.editForm.value)
      .subscribe((results) => {
        this.ngOnInit();
        this.modalService.dismissAll();
      });
  }

   addItems():void{
      this.authService.addNew(this.userForm.value).subscribe(result => {
        console.log(result);
        alert(
          "data added"
        )
      })
      this.guestList.push(this.userForm.value);
      this.userForm.reset();

     this.modalService.dismissAll();
   }

  ngOnInit(): void {

    this.getList();
    this.editForm = this.builder.group({
      GuestId: [''],
      Name: [''],
      PhoneNo: [''],
      Email: [''],
      IdNo: [''],
      DateOfBirth: [''],
      IsActive:['']
    } );
  }

  getList() {
    this.authService.guestList().subscribe((displayList) => {

      this.guestList = displayList;
      this.pageLength = displayList.length;
    },
      (error) => { console.log("error") }
    );
  }
 test(){
   console.log("hiiiii")
 }
  open(content: any) {
    console.log('hi')
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  openDelete(targetModal: any, guestId: any) {
    this.deleteID = guestId;
    const modal = this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });
  }

  onDelete(deleteID: any) {
    console.log("id", this.deleteID)
    this.guestList.splice(this.deleteID - 1, 1)
    this.authService.delGuest(deleteID).subscribe((result)=>{
      console.log(result);
    
    })
    this.modalService.dismissAll();

  }
}


