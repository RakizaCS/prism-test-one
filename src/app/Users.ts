export class Reservation{
    
    id!:string;
    ReservationNo!:string;
    GuestId!:string;
    RoomId!:string;
    RoomTypeId!:string;
    ArrivalDate!:string;
    DepartureDate!:string;
    Notes!:string;
    Name!:string;
}

export class Rooms{
    
    id!:string;
    RoomNo!:string;
    RoomTypeId!:string;
    Price!:string;
    RoomStatus!:string;
    IsActive!:boolean;
}

export class Guest{
    
    GuestId!:string;
    Name!:string;
    PhoneNo!:string;
    Email!:string;
    IdNo!:string;
    DateOfBirth!:string;
    IsActive!:boolean;
}