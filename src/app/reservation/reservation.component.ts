import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Reservation } from '../Users';
import { Guest } from '../Users';
import { map, switchMap } from 'rxjs/operators';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http'; 


@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss']
})

export class ReservationComponent implements OnInit {

  dataList: Reservation[] = [];
  guestList: Guest[] = [];
  guestIdName?: any;
  deleteID!: number;
  closeResult!: string;
  userForm!:FormGroup;
  editID!: number;
  editForm!:FormGroup
  pageLength!:any;
  page: number = 1;
  paginator: any;

  constructor(private authService:AuthService, private modalService: NgbModal, private builder:FormBuilder, private http:HttpClient) {
    this.userForm = this.builder.group({
      ReservationNo:['',Validators.required],
      GuestId:['',Validators.required],
      RoomId:['',Validators.required],
      RoomTypeId:['',Validators.required],
      ArrivalDate:['',Validators.required],
      DepartureDate:['',Validators.required],
      Notes:['',Validators.required]
    })

    this.editForm = this.builder.group({
      id: [''],
      ReservationNo: [''],
      GuestId: [''],
      RoomId: [''],
      RoomTypeId: [''],
      ArrivalDate: [''],
      DepartureDate: [''],
      Notes:['']
    } );

   }
 
  ngOnInit(): void {
    this.getReserveList();
    this.gerMergeData();
    this.getGuestId();
    // this.getMergeGuestName();
    this.editForm = this.builder.group({
      id: [''],
      ReservationNo: [''],
      GuestId: [''],
      RoomId: [''],
      RoomTypeId: [''],
      ArrivalDate: [''],
      DepartureDate: [''],
      Notes:['']
    } );
  }

  gerMergeData(){​​​​​​​​
    this.authService.reservationList().pipe(
          switchMap(dataList =>this.authService.guestList().pipe(map(guestList => {​​​​​​​​
            dataList.forEach(order=>{​​​​​​​​
    const guest = guestList.find(guest=> guest.GuestId === order.GuestId);
    order.GuestId  = guest ? guest.Name : '';
      
          }​​​​​​​​);
    return dataList;
          }​​​​​​​​))),
        ).subscribe(
          (response:any) => {​​​​​​​​
            this.dataList = response;
            console.log("AAA" , this.dataList)
            
          }​​​​​​​​,
          (error) =>{​​​​​​​​
            console.log(error);
          }​​​​​​​​
        );
        
      }

  getReserveList(){
    
    this.authService.reservationList().subscribe((reserveData) => {
       
          this.dataList = reserveData;
          this.pageLength = reserveData.length;  
      },

      (error) => { console.log("error")}
    );
  }

  getGuestId(){
    
    this.authService.guestList().subscribe((guestId) => {
          this.guestList = guestId;
      },

      (error) => { console.log("error")}
    );
  }



  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  open(content: any) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  addItems():void{
    this.authService.addNewReservation(this.userForm.value).subscribe(result => {
      console.log(result);
      // alert(
      //   "data added"
      // )
    })
    console.log(this.userForm.value)

    this.dataList.push(this.userForm.value);
    this.userForm.reset();
   this.modalService.dismissAll();
   

 }

  // Delete reservation details - open Modal

  openDelete(targetModal: any, guestId: any) {
    this.deleteID = guestId;
    console.log(this.deleteID);
    const modal = this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });
  }

  // Delete reservation details 

  onDelete(deleteID: any) {
    console.log("id", this.deleteID)
    this.dataList.splice(this.deleteID - 1, 1)
    this.authService.delReservation(deleteID).subscribe((result)=>{
      console.log(result);
    
    })
    this.modalService.dismissAll();

  }

  // Reservation edit - open Modal

  openEdit(targetModal: any, reserve: Reservation ) {
    console.log("Return : " ,  reserve)
    
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });

    this.editForm.patchValue( {
      id: reserve.id,
      ReservationNo: reserve.ReservationNo, 
      GuestId: reserve.GuestId,
      RoomId: reserve.RoomId,
      RoomTypeId: reserve.RoomTypeId,
      ArrivalDate: reserve.ArrivalDate,
      DepartureDate: reserve.DepartureDate,
      Notes: reserve.Notes
    });
    
  }

  // Reservation edit - save edit

  saveEdit(guestId: any){
  const editURL = 'http://localhost:3000/orders/'+ this.editForm.value.id ;
  console.log(this.editForm.value);
  this.http.put(editURL, this.editForm.value)
    .subscribe((results) => {
      this.modalService.dismissAll();
      console.log(this.dataList)
      
    });
    this.ngOnInit();
  }

  //check - (In/Out) button

  toggle = true;
  status = 'Enable';

  statusCheck(targetModal: any, deleteID:any){
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'lg'
    });
    
  }
  onConfirm(deleteID: any) {

      this.toggle = !this.toggle;
      this.status = this.toggle ? 'Enable' : 'Disable';
    
    this.modalService.dismissAll();

  }
}

